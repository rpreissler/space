require 'test_helper'

class ElevatorsControllerTest < ActionController::TestCase
  setup do
    @elevator = elevators(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:elevators)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create elevator" do
    assert_difference('Elevator.count') do
      post :create, elevator: { customer_id: @elevator.customer_id, number: @elevator.number }
    end

    assert_redirected_to elevator_path(assigns(:elevator))
  end

  test "should show elevator" do
    get :show, id: @elevator
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @elevator
    assert_response :success
  end

  test "should update elevator" do
    patch :update, id: @elevator, elevator: { customer_id: @elevator.customer_id, number: @elevator.number }
    assert_redirected_to elevator_path(assigns(:elevator))
  end

  test "should destroy elevator" do
    assert_difference('Elevator.count', -1) do
      delete :destroy, id: @elevator
    end

    assert_redirected_to elevators_path
  end
end
