Rails.application.routes.draw do
  resources :activities, only: [:index]

  resources :roles

  resources :suppliers

  namespace :dashboard do
    resources :tasks, :only => [:today]

   resources :time_entries
 end

  get 'dashboard/index'
  get 'dashboard/tasks' => 'dashboard#tasks'
  get 'dashboard/tasks/finished' => 'dashboard#finished'
  get 'dashboard/tasks/open' => 'dashboard#open'
  get 'dashboard/tasks/active' => 'dashboard#active'
  get 'dashboard/tasks/priority' => 'dashboard#priority'
  get 'dashboard/timeentries' => 'dashboard#time_entries'
  get 'dashboard/timeentries/week' => 'dashboard#this_week'
  get 'dashboard/timeentries/month' => 'dashboard#this_month'
  get 'dashboard/users' => 'dashboard#users'

  resources :invoices, :as => :documents, :controller => :documents do
    member do
      patch :unlock
    end

    collection do
      get :locked
      get :unlocked
      get :unpaid
    end
  end

  resources :projects do
    resources :invoices, :as => :documents

  end

  resources :customers

  resources :elevators

  resources :time_entries

  get 'pages/home' # /root page for signed_in users
  get 'stats' => 'pages#statistics' # /stats page

  devise_for :users
  as :user do
    get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
    put 'users' => 'devise/registrations#update', :as => 'user_registration'
  end
  scope "/dashboard" do
    resources :users
  end

  resources :tasks do
    member do
      patch :complete
    end
    resources :time_entries
    # Routes for scopes
    collection do
      get :open
      get :active
      get :finished
      get :today
    end
  end

  #is user signed_in routes to
  #authenticated :user do
  #  root 'pages#home', as: 'authenticated_root'
  #end
  #is user not signed_in routes to login path

  authenticated :user, lambda {|u| u.has_role? :admin}  do
    root to: "dashboard#index", as: :admin_root
  end

  authenticated :user, lambda {|u| u.has_role? :manager} do
    root to: "tasks#index", as: :manager_root
  end

  authenticated :user, lambda {|u| u.has_role? :accountant} do
    root to: "documents#index", as: :accountant_root
  end

  # all other roles
  authenticated :user do
    root 'pages#home', as: 'authenticated_root'
  end

  # is user not signed_in routes to login path
  root :to => redirect('/users/sign_in')
end
