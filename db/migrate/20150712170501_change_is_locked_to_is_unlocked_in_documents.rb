class ChangeIsLockedToIsUnlockedInDocuments < ActiveRecord::Migration
  def change
    rename_column :documents, :is_locked, :is_unlocked
  end
end
