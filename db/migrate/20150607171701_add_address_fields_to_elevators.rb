class AddAddressFieldsToElevators < ActiveRecord::Migration
  def change
    add_column :elevators, :city, :string
    add_column :elevators, :postal_code, :string
  end
end
