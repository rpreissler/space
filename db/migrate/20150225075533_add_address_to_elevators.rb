class AddAddressToElevators < ActiveRecord::Migration
  def change
    add_column :elevators, :address, :string
  end
end
