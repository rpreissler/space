class AddHasBreakToTimeEntries < ActiveRecord::Migration
  def change
    add_column :time_entries, :break, :float, :default => 0
  end
end
