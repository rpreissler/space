class AddDocumentableToDocuments < ActiveRecord::Migration
  def change
    add_reference :documents, :documentable, polymorphic: true, index: true
  end
end
