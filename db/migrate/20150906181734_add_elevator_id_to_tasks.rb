class AddElevatorIdToTasks < ActiveRecord::Migration
  def change
    add_reference :tasks, :elevator, index: true
  end
end
