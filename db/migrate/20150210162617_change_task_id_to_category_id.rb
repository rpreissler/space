class ChangeTaskIdToCategoryId < ActiveRecord::Migration
  def change
    rename_column :tasks, :task_id, :category_id
  end
end
