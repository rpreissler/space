class AddDefaultValueToStatusId < ActiveRecord::Migration
  def change
    change_column :tasks, :status_id, :integer, :default => 1
  end
end
