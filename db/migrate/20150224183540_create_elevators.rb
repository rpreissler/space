class CreateElevators < ActiveRecord::Migration
  def change
    create_table :elevators do |t|
      t.string :number
      t.integer :customer_id

      t.timestamps
    end
  end
end
