class ChangeAddressColumnToStreetInElevators < ActiveRecord::Migration
  def change
    rename_column :elevators, :address, :street
  end
end
