class AddUserRefToTimeEntries < ActiveRecord::Migration
  def change
    add_reference :time_entries, :user, index: true
  end
end
