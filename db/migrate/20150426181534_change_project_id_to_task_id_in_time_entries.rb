class ChangeProjectIdToTaskIdInTimeEntries < ActiveRecord::Migration
  def change
    rename_column :time_entries, :project_id, :task_id 
  end
end
