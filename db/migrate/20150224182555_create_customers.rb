class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.string :address
      t.string :postal_code
      t.string :city
      t.string :contact_form
      t.string :contact_name

      t.timestamps
    end
  end
end
