class ChangeDefaultValueForHoursInTimeEntries < ActiveRecord::Migration
  def change
    change_column :time_entries, :hours, :integer, default: 0
  end
end
