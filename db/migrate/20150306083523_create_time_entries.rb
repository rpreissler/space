class CreateTimeEntries < ActiveRecord::Migration
  def change
    create_table :time_entries do |t|
      t.references :project, index: true
      t.integer :hours
      t.date :date
      t.text :description

      t.timestamps
    end
  end
end
