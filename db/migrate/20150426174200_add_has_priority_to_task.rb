class AddHasPriorityToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :has_priority, :boolean, :default => false
  end
end
