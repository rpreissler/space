class ChangeIsUnlockedDefaultInDocuments < ActiveRecord::Migration
  def change
    change_column_default(:documents, :is_unlocked, 'false')
  end
end
