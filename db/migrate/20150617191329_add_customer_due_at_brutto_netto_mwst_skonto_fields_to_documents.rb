class AddCustomerDueAtBruttoNettoMwstSkontoFieldsToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :due_at, :date
    add_column :documents, :brutto, :decimal, precision: 11, scale: 2
    add_column :documents, :netto, :decimal, precision: 11, scale: 2
    add_column :documents, :has_skonto, :boolean, :default => false
    add_column :documents, :tax, :decimal
    add_reference :documents, :customer, index: true
  end
end
