class AddCaretakerToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :caretaker, :string
  end
end
