class ChangeCustomerIdToSupplierId < ActiveRecord::Migration
  def change
    rename_column :documents, :customer_id, :supplier_id
  end
end
