class ChangeBudgetToPlannedHoursInProjects < ActiveRecord::Migration
  def change
     rename_column :projects, :budget, :planned_hours
  end
end
