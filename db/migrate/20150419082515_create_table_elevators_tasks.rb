class CreateTableElevatorsTasks < ActiveRecord::Migration
  def change
    create_table :elevators_tasks, :id => false do |t|
      t.references :task
      t.references :elevator
    end
  end
end
