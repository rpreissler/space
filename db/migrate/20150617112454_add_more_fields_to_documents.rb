class AddMoreFieldsToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :is_locked, :boolean, :default => true
    add_column :documents, :is_paid, :boolean, :default => false
  end
end
