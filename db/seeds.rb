# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# ruby encoding: utf-8


roles = [
  [ 'admin', '2015-02-23 18:06:40.033363', '2015-02-23 18:06:40.033363', 'Administrator: Besitzt alle Funktionen.'],
  [ 'manager', '2015-02-23 18:06:40.033363', '2015-02-23 18:06:40.033363', 'Büromitarbeiter: Besitzt erweiterte Funktionen.'],
  [ 'employee', '2015-02-23 18:06:40.033363', '2015-02-23 18:06:40.033363', 'Mitarbeiter: Besitzt einfache Funktionen.'],
  [ 'accountant', '2015-02-23 18:06:40.033363', '2015-02-23 18:06:40.033363', 'Buchhaltung: Besitzt Funktionen fürs Rechnungswesen.']
]

roles.each do |role|
  Role.create( :name => role[0], :created_at => role[1], :updated_at => role[2], :description => role[3])
end


users = [
  [ 'admin@crm.de', "12345678", '2015-02-23 18:06:40.033363', '2015-06-12 10:18:45.639347', 'John', 'Doe', 1 ],
  [ 'manager@crm.de', "12345678", '2015-02-23 18:06:40.033363', '2015-06-12 10:18:45.639347', 'Max', 'Mustermann', 2 ],
  [ 'employee@crm.de', "12345678", '2015-02-23 18:06:40.033363', '2015-06-12 10:18:45.639347', 'Janine', 'Jane', 3 ],
  [ 'accountant@crm.de', "12345678", '2015-02-23 18:06:40.033363', '2015-06-12 10:18:45.639347', 'Stephanie', 'Hoffmann', 4 ]
]

users.each do |user|
  User.create( :email => user[0], :password => user[1], :password_confirmation => user[1], :created_at => user[2], :updated_at => user[3], :first_name => user[4], :last_name => user[5], :role_ids => user[6] )
end


categories = [
  [ "Auftragsarbeit" ],
  [ "Nachweis" ],
  [ "Neubau" ],
  [ "Reparatur" ],
  [ "Störung" ],
  [ "Teilwartung" ],
  [ "TÜV" ],
  [ "Umbau" ],
  [ "Vandalismus" ],
  [ "Vollwartung" ],
  [ "Sonstiges" ]
]

categories.each do |categorie|
  Category.create( :name => categorie[0] )
end

statuses = [
  [ "offen" ],
  [ "in Arbeit" ],
  [ "erledigt" ]
]

statuses.each do |status|
  Status.create( :name => status[0] )
end
