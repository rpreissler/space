module ApplicationHelper
  def number_precision(number)
    number_with_precision(number, precision: 2, separator: ',', delimiter: '.')
  end

  # Returns the full title on a per-page basis.
  def pages_title(page_title)
    base_title = "CRM"    # Variable assignment
    if page_title.empty?
      base_title
    else
      raw("#{page_title}")                   # String interpolation
    end
  end

  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = (column == sort_column) ? "current #{sort_direction}" : nil
    direction = (column == sort_column && sort_direction == "asc") ? "desc" : "asc"
    link_to title, {:sort => column, :direction => direction}, {:class => css_class}
  end

  # highlight link based on current page
  def active_class?(*paths)
    active = false
    paths.each { |path| active ||= current_page?(path) }
    active ? 'active' : nil
  end
  # Here's how you'd use it:
  # <%= link_to "Bookings", bookings_path, class: active_class?(bookings_path) %>
  # You can pass multiple paths to it in case you have a tab which could be rendered by multiple views:
  # <%= content_tag :li, class: active_class?(bookings_path, action: 'new') %>

  def sort_link()
  end
end
