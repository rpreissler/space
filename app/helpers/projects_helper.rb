module ProjectsHelper

  def full_hours
    @project.time_entries.sum(:hours)
  end

  def hours(start_time, end_time, pause)
    ((end_time - start_time) / 3600).round - pause
  end

  def total_hours
    @project.time_entries.hours.sum
  end
end
