module TasksHelper
  def assignee
    @task.assignments.map(&:user_id).include? current_user.id
  end
end
