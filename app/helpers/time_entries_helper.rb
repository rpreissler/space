module TimeEntriesHelper

  def full_hours
    @time_entries.sum(:hours)
  end

  def date_range(end_date, start_date, pause)
    if end_date && start_date
      if pause > 0
        ((end_date - start_date) / 3600) - pause
      else
        ((end_date - start_date) / 3600)
      end
    else
      0 # fallback value
    end
  end

  def break_in_words(pause)
    case pause
    when 0.5
      "30 Minuten"
    when 0.75
      "45 Minuten"
    when 1
      "1 Stunde"
    else
      "keine Pause"
    end
  end

end
