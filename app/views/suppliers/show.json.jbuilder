json.extract! @supplier, :id, :name, :contact_form, :contact_name, :address, :postal_code, :city, :region, :phone, :fax, :email, :website, :created_at, :updated_at
