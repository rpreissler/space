json.array!(@suppliers) do |supplier|
  json.extract! supplier, :id, :name, :contact_form, :contact_name, :address, :postal_code, :city, :region, :phone, :fax, :email, :website
  json.url supplier_url(supplier, format: :json)
end
