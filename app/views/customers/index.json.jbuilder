json.array!(@customers) do |customer|
  json.extract! customer, :id, :name, :address, :postal_code, :city, :contact_form, :contact_name
  json.url customer_url(customer, format: :json)
end
