json.array!(@elevators) do |elevator|
  json.extract! elevator, :id, :number, :customer_id
  json.url elevator_url(elevator, format: :json)
end
