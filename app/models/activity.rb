class Activity < ActiveRecord::Base
  belongs_to :user
  belongs_to :targetable, polymorphic: true

  scope :today, lambda { where("created_at BETWEEN '#{DateTime.now.beginning_of_day}' AND '#{DateTime.now.end_of_day}'")  }
  scope :recent, ->(num) { order('created_at DESC').limit(num) }
end
