# == Schema Information
#
# Table name: time_entries
#
#  id          :integer          not null, primary key
#  project_id  :integer          not null
#  hours       :integer          not null
#  date        :date             not null
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class TimeEntry < ActiveRecord::Base
  belongs_to :task, touch: true
  belongs_to :user, touch: true
  before_save :save_hours

  validate :start_time_cannot_be_after_end_time
  #validates :hours, presence: true, numericality: { greater_than: 0, only_integer: true }

  scope :this_week, lambda { where(:date => Date.today.beginning_of_week..Date.today.end_of_week) }
  scope :this_month, lambda { where(:date => Date.today.beginning_of_month..Date.today.end_of_month) }


  def start_time_cannot_be_after_end_time
    errors.add(:start_time, "Startzeit muss vor Endzeit sein") if start_time >= end_time
  end

  def self.as_csv
    CSV.generate do |csv|
      csv << column_names
      all.each do |item|
        csv << item.attributes.values_at(*column_names)
      end
    end
  end

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |time_entry|
        csv << time_entry.attributes.values_at(*column_names)
      end
    end
  end

  private

  def save_hours
    @break = self.break
    self.hours = ((end_time - start_time) / 3600).round - @break

  end

end
