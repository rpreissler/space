# == Schema Information
#
# Table name: suppliers
#
#  id             :integer          not null, primary key
#  name           :varchar
#  contact_form   :varchar
#  contact_name   :varchar
#  address        :varchar
#  postal_code    :varchar
#  city           :varchar
#  region         :varchar
#  phone          :varchar
#  fax            :varchar
#  email          :varchar
#  website        :varchar
#  created_at     :datetime
#  updated_at     :datetime
#

class Supplier < ActiveRecord::Base

  validates :name, presence: true, length: { minimum: 3 }, uniqueness: true
  validates :address, presence: true
  validates :postal_code, presence: true
  validates :city, presence: true

  validates_format_of :postal_code, :with => /\A\d{5}\z/,
                                    message: "Postleitzahl muss so aussehen: 12345"

  has_many :documents

end
