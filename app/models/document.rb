class Document < ActiveRecord::Base
  resourcify

  has_attached_file :attachment, styles: { small: "64x64", medium: "100x100", large: "200x200" }

  before_post_process :skip_for_non_image
  validates_attachment_content_type :attachment, :content_type => ['image/jpeg', 'image/png', 'application/pdf']

  belongs_to :supplier

  belongs_to :documentable, polymorphic: true
  belongs_to :project
  belongs_to :task

  validates :attachment,
            :brutto,
            :due_at,
            :netto,
            :number,
            :supplier_id,
                        presence: true

  def skip_for_non_image
    ! %w(application/pdf).include?(attachment_content_type)
  end

  def documentable_type=(class_name)
     super(class_name.constantize.base_class.to_s)
  end

  scope :locked,                -> { where(is_unlocked: false) }
  scope :unlocked,              -> { where(is_unlocked: true) }
  scope :unlocked_and_not_paid, -> { unlocked.where(is_paid: false) }
  scope :priority,              -> { where(has_priority: true)}

end
