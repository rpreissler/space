class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :roles, presence: true

  has_many :activities
  has_many :assignments
  has_many :tasks, :through => :assignments
  has_many :time_entries

  has_many :users_roles
  has_many :roles, :through => :users_roles

  rolify

  def full_name
    first_name + " " + last_name
  end

  def has_role?(role_sym)
    roles.any? { |r| r.name.underscore.to_sym == role_sym }
  end

  def create_activity(item, action)
    activity = activities.new
    activity.targetable = item
    activity.action = action
    activity.save
    activity
  end
end
