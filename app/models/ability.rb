class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new # in case of guest

    if user.has_role? :admin
      can :manage, :all
      cannot :create, TimeEntry
      cannot :edit, TimeEntry
      cannot :delete, TimeEntry
    elsif user.has_role? :manager
      can :manage, Customer
      can :manage, Task
      can :manage, Project
      can :manage, Elevator
      can :manage, Supplier
      can :create, Document
      can :show, Document
      can :read, Document
      can :show, TimeEntry
      can :read, TimeEntry
      cannot :create, TimeEntry
    elsif user.has_role? :accountant
      can :read, Document, is_unlocked: true
    elsif user.has_role? :employee
      can :manage, TimeEntry
      can :show, Customer
      can :show, Elevator
      can :read, Task
      can :open, Task
      can :complete, Task
      can :finished, Task
      can :active, Task
      can :update, Task, id: user.tasks.pluck(:id)
      cannot :read, Dashboard
      can :read, Project
    end


    # Define abilities for the passed in user here. For example:
    #
    # user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #    can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
