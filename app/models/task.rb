# Copyright (c) 2015 Romano Preißler.
#
# PRISM CRM
#------------------------------------------------------------------------------
# == Schema Information
#
# Table name: tasks
#
#  id              :integer         not null, primary key
#  title           :varchar(255)
#  description     :text
#  created_at      :datetime
#  updated_at      :datetime
#  categoriy_id    :integer
#  user_id         :integer
#  status_id       :integer
#
#

class Task < ActiveRecord::Base
  resourcify

  belongs_to :category
  belongs_to :status
  belongs_to :user
  belongs_to :project
  #belongs_to :document
  belongs_to :elevator

  has_many :documents, :as => :documentable

  validates :elevator_id, presence: true
  validates :due_at, presence: true

  # Status Filter
  scope :open,      -> { where(status: 1) }
  scope :active,    -> { where(status: 2) }
  scope :finished,  -> { where(status: 3) }
  scope :priority,  -> { where(has_priority: true) }
  scope :fault,     -> { where(category_id: 2 )}


  scope :unfinished,-> { where.not(status: 3) }
  scope :today, lambda { where("completed_at BETWEEN '#{DateTime.now.beginning_of_day}' AND '#{DateTime.now.end_of_day}'")  }

  has_many :assignments, dependent: :destroy
  has_many :users, :through => :assignments

  has_many :time_entries
  accepts_nested_attributes_for :time_entries
  accepts_nested_attributes_for :documents, :allow_destroy => true, :reject_if => proc {|attributes|
    attributes.all? {|k,v| v.blank?}
  }




  def self.as_csv
    CSV.generate do |csv|
      csv << column_names
      all.each do |item|
        csv << item.attributes.values_at(*column_names)
      end
    end
  end

end
