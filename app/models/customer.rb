class Customer < ActiveRecord::Base
  has_many :elevators
  has_many :projects

  validates_format_of :postal_code, :with => /([0124678][0-9]{4})/i, :on => :create, :message => "Keine gültige Postleitzahl"

  validates :name, presence: true, length: { minimum: 3 }, uniqueness: true
  validates :address, presence: true
  validates :postal_code, presence: true,
                          length: { is: 5 }
  validates :city, presence: true

  def contact
    contact_form + ' ' + contact_name
  end

end
