class Elevator < ActiveRecord::Base
  belongs_to :customer
  has_many :tasks

  validates_format_of :postal_code, :with => /([0124678][0-9]{4})/i, :on => :create, :message => "Keine gültige Postleitzahl"

  validates :number, presence: true
  validates :customer_id, presence: true
  validates :street, presence: true
  validates :postal_code, presence: true,
                          length: { is: 5 }
  validates :city, presence: true

  def address
    street + ', ' + postal_code + ' ' + city
  end
end
