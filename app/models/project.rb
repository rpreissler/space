class Project < ActiveRecord::Base

  validates :name, presence: true, length: { minimum: 3 }, uniqueness: true
  validates :description, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :customer_id, presence: true

  validate :end_date_is_after_start_date

  has_many :tasks
  has_many :time_entries, through: :tasks

  belongs_to :customer
  #belongs_to :document

  has_many :documents, :as => :documentable
  accepts_nested_attributes_for :documents, :allow_destroy => true, :reject_if => proc {|attributes|
    attributes.all? {|k,v| v.blank?}
  }

  #accepts_nested_attributes_for :document

  def address
    street + ' ' + postal_code + ' ' + city
  end

  private

  def end_date_is_after_start_date
    return if end_date.blank? || start_date.blank?

    if end_date <= start_date
      errors.add(:end_date, "Wähle ein Datum aus welches nach dem Startdatum ist")
    end
  end
end
