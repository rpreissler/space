class TimeEntriesController < ApplicationController

  before_action :set_time_entry, only: [:show, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update, :destroy] # only the time entry creator can edit, update and destroy the time entry

  before_filter :load_task
  respond_to :html


  def index
    @time_entries = TimeEntry.all
  end

  def new
    @time_entry = TimeEntry.new
    @time_entry = current_user.time_entries.build # set the user id in new time entry
  end

  def edit
  end

  def create
    #@time_entry = TimeEntry.new(timeentry_params)
   # @time_entry = current_user.time_entries.build(timeentry_params) # set the user id in new time entry
    @time_entry = @task.time_entries.build(timeentry_params)
    @time_entry.user_id = current_user.id
 # redirect_to user_post_path(@user.id,@post)
    if @time_entry.save
      redirect_to @task, notice: "Arbeitsstunden hinzugefügt."
    else
      render "new"
    end
  end

  def update
    @time_entry.update(timeentry_params)
    respond_with(@time_entry)
  end

  def show
    @time_entry = TimeEntry.find(params[:id])
  end

  def destroy
    @time_entry.destroy
    respond_with(@time_entry)
  end

  private

  def correct_user
    @time_entry = current_user.time_entries.find_by(id: params[:id])
    redirect_to time_entries_path, notice: "Sie sind nicht authorisiert die Arbeitsstunden zu bearbeiten." if @time_entry.nil?
  end

  def set_time_entry
    @time_entry = TimeEntry.find(params[:id])
  end

  def timeentry_params
    params.require(:time_entry).permit(:task_id, :hours, :date, :description, :start_time, :end_time, :break)
  end

  def load_task
    @task = Task.find(params[:task_id])
  end

end
