class DashboardController < ApplicationController
  load_and_authorize_resource #cancancan authorize method
  helper_method :sort_column, :sort_direction
  def index
    @tasks_completed = Task.where(status_id: 3).count
    @category_maintenance_id = Category.find_by(name: "Teilwartung").id
    @tasks_maintenance = Task.where(category_id: @category_maintenance_id)
    @tasks_maintenance_count = @tasks_maintenance.count
    @tasks_maintenance_open = @tasks_maintenance.where(status_id: 1).count
    @category_fault_id = Category.find_by(name: "Störung").id
    @tasks_fault = Task.where(category_id: @category_fault_id)
    @tasks_fault_open = @tasks_fault.where(status_id: 1).count
    @category_repair_id = Category.find_by(name: "Reparatur").id
    @tasks_repair = Task.where(category_id: @category_repair_id)
    @tasks_repair_open = @tasks_repair.where(status_id: 1).count
  end

  def tasks
    @tasks = Task.all.order(sort_column + ' ' + sort_direction)
    @elevators = Elevator.all
    respond_to do |format|
      format.html
      format.csv { send_data @tasks.as_csv }
    end
  end

  def time_entries
    @time_entries = TimeEntry.all
    respond_to do |format|
      format.html
      if params[:time_entries_data] == 'this_week'
        format.csv { send_data @time_entries.this_week.as_csv }
      elsif params[:time_entries_data] == 'this_month'
        format.csv { send_data @time_entries.this_month.as_csv }
      elsif
        format.csv { send_data @time_entries.as_csv }
      end
      format.xls
    end
  end

  def users
    @users = User.all
  end

  # shows all time entries of the current week
  def this_week
    @time_entries = TimeEntry.this_week
    render action: :time_entries
  end

  # shows all time entries of the current month
  def this_month
    @time_entries = TimeEntry.this_month
    render action: :time_entries
  end


  # shows all tasks there status is 1
  # 1 is the default value for tasks
  def open
    @tasks = Task.open.order(sort_column + ' ' + sort_direction)
    render action: :tasks
  end

  # shows all tasks there status is 2
  def active
    @tasks = Task.active.order(sort_column + ' ' + sort_direction)
    render action: :tasks
  end

  # shows all tasks there status is 3
  def finished
    @tasks = Task.finished.order(sort_column + ' ' + sort_direction)
    render action: :tasks
  end

  # shows all tasks there status is 3
  def priority
    @tasks = Task.unfinished.priority.order(sort_column + ' ' + sort_direction)
    render action: :tasks
  end



  private

  # default sort column
  def sort_column
    Task.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end

  # default sort direction
  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "desc"
  end

end
