class DocumentsController < ApplicationController
  load_and_authorize_resource #cancancan authorize method
  helper_method :sort_column, :sort_direction

  respond_to :html


  def index
    @documents = Document.all.order(sort_column + ' ' + sort_direction).paginate(page: params[:page], per_page: 7)
    respond_with(@documents)
  end

  def show
    @document = Document.find(params[:id])
  end

  def new
    @document = Document.new
    respond_with(@document)
  end

  def edit
    @document = Document.find(params[:id])
  end

  def create
    @document = Document.new(document_params)
    documentable_params = params[:document][:documentable].match(/^(?<type>\w+):(?<id>\d+)$/)
    params[:document].delete(:documentable)
    @document.documentable_id         =  documentable_params[:id]
    @document.documentable_type       =  documentable_params[:type]

    @document.user_id = current_user.id
    if @document.save
      current_user.create_activity(@document, 'created') # create file activity
      redirect_to @document
    else
      render "new"
    end
  end

  def update
    @document = Document.find(params[:id])
    @document.update(document_params)
    current_user.create_activity(@document, 'updated') # updated document activity
    respond_with(@document)
  end

  def unlock
    @document.update_attributes(is_unlocked: true)
    @document.save
    redirect_to @document, notice: "Diese Rechnung ist nun für das Büro sichtbar."
  end

  def destroy
    @document.attachment = nil
    @document.save
  end


  # show all documents there are locked
  def locked
    @documents = @documents.locked.order(sort_column + ' ' + sort_direction).paginate(page: params[:page], per_page: 7)
    render action: :index
  end

  # show all documents there are unlocked
  def unlocked
    @documents = @documents.unlocked.order(sort_column + ' ' + sort_direction).paginate(page: params[:page], per_page: 7)
    render action: :index
  end

  # show all documents there are unlocked and not paid
  def unpaid
    @documents = @documents.unlocked_and_not_paid.order(sort_column + ' ' + sort_direction).paginate(page: params[:page], per_page: 7)
    render action: :index
  end


  private
    def set_document
      @document = Document.find(params[:id])
    end

    def document_params
      params.require(:document).permit(:attachment, :is_unlocked, :is_paid, :brutto, :netto, :has_skonto, :supplier_id, :due_at, :tax, :number, :documentable_id, :documentable_type)
    end

    # default sort column
    def sort_column
      Document.column_names.include?(params[:sort]) ? params[:sort] : "number"
    end

    # default sort direction
    def sort_direction
      %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
    end

end
