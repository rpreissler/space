class TasksController < ApplicationController

  load_and_authorize_resource


  def index
    if params[:category].blank?
      @tasks = Task.all.order("due_at DESC")
      @task_months = @tasks.group_by{ |t| t.due_at.beginning_of_month }
      @task_days = @tasks.group_by{ |t| t.due_at.beginning_of_day }
    else
      # Filter for categories
      @category_id = Category.find_by(name: params[:category]).id
      @tasks = Task.where(category_id: @category_id).order("created_at DESC")
      @task_months = @tasks.group_by{ |t| t.due_at.beginning_of_month }
      @task_days = @tasks.group_by{ |t| t.due_at.beginning_of_day }
    end
  end

  def show
    @time_entry = TimeEntry.new
    @activities = Activity.where(targetable_type: "Task")
    @activities = @activities.where(targetable_id: @task.id)
  end

  def new
    @task = current_user.tasks.build
    @task.build_document
  end


  # shows all tasks there status is 1
  # 1 is the default value for tasks
  def open
    if params[:category].blank?
      @tasks = current_user.tasks.open
      @task_months = @tasks.group_by{ |t| t.due_at.beginning_of_month }
      @task_days = @tasks.group_by{ |t| t.due_at.beginning_of_day }
      render action: :index
    else
      # Filter for categories
      @category_id = Category.find_by(name: params[:category]).id
      @tasks = current_user.tasks.where(category_id: @category_id).order("created_at DESC")
      @task_months = @tasks.group_by{ |t| t.due_at.beginning_of_month }
      @task_days = @tasks.group_by{ |t| t.due_at.beginning_of_day }
      render action: :index
    end
  end

  # shows all tasks there status is 2
  def active
    @tasks = current_user.tasks.active
    @task_months = @tasks.group_by{ |t| t.due_at.beginning_of_month }
    @task_days = @tasks.group_by{ |t| t.due_at.beginning_of_day }
    render action: :index
  end

  # shows all tasks there status is 3
  def finished
    @tasks = current_user.tasks.finished
    render action: :finished
  end

  def create
    @task = current_user.tasks.build(task_params)
    @task.user_id = current_user.id

    if @task.save
      current_user.create_activity(@task, 'created') # create task activity
      redirect_to @task
    else
      render "new"
    end
  end

  def edit
    @task.documents.build      # insert a new 'create document field'
  end

  def update
    respond_to do |format|
      if @task.update(task_params)
        if @task.status_id == 3
          current_user.create_activity(@task, 'finished') # finished task activity
          format.html { redirect_to @task, notice: "#{@task.category.name} wurde erfolgreich abgeschlossen." }
        elsif @task.status_id == 2
          current_user.create_activity(@task, 'progress') # progressed task activity
          format.html { redirect_to @task, notice: "#{@task.category.name} ist in Bearbeitung." }
        else
          current_user.create_activity(@task, 'updated') # update task activity
          format.html { redirect_to @task, notice: 'Die Aufgabe wurde bearbeitet.' }
          format.json { render :show, status: :ok, location: @task }
        end
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @task.destroy
    respond_to do |format|
      current_user.create_activity(@task, 'deleted') # finished task activity
      format.html { redirect_to dashboard_tasks_url, notice: 'Die Aufgabe wurde gelöscht.'}
      format.json { head :no_content }
    end
  end

  def complete
    @task.update_attributes(completed_at: Time.now, status_id: 3)
    current_user.create_activity(@task, 'finished') # finished task activity
    @task.save
    redirect_to @task, notice: "Die Aufgabe ist erledigt."
  end

  private

  def find_task
    @task = Task.find(params[:id])
  end

  def correct_user
    @task = current_user.tasks.find_by(id: params[:id])
    redirect_to tasks_path, notice: "Sie haben keine Berechtigung um diese Aufgabe zu bearbeiten." if @task.nil?
  end

  def task_params
    params.require(:task).permit(:description, :user_id, :category_id, :project_id, :has_priority, :status_id, :due_at, :elevator_id, documents_attributes: [:id, :brutto, :netto, :number, :due_at, :attachment, :supplier_id], :user_ids => [] )
  end


end
