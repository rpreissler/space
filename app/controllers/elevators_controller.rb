class ElevatorsController < ApplicationController
  load_and_authorize_resource #cancancan authorize method


  respond_to :html

  def index
    @elevators = Elevator.all
    respond_with(@elevators)
  end

  def show
    respond_with(@elevator)
  end

  def new
    @elevator = Elevator.new
    respond_with(@elevator)
  end

  def edit
  end

  def create
    @elevator = Elevator.new(elevator_params)
    @elevator.save
    respond_with(@elevator)
  end

  def update
    @elevator.update(elevator_params)
    respond_with(@elevator)
  end

  def destroy
    @elevator.destroy
    respond_with(@elevator)
  end

  private
    def set_elevator
      @elevator = Elevator.find(params[:id])
    end

    def elevator_params
      params.require(:elevator).permit(:number, :customer_id, :street, :city, :postal_code)
    end
end
