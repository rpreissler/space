class PagesController < ApplicationController
  def home
    # only current user task there are not completed from today and older
    @tasks = current_user.tasks.where('DATE(due_at) <= ? AND status_id != 3', Date.today).order("due_at DESC")
    #@tasks = current_user.tasks.order("due_at DESC") # sort current user task at due at
    @tasks_now_to_future = current_user.tasks.where('DATE(due_at) >= ? AND status_id != 3 AND has_priority = false', Date.today).order("due_at ASC")
    @task_days = @tasks_now_to_future.group_by{ |t| t.due_at.beginning_of_day }
    @time_entries = current_user.time_entries.all
    @tasks_finished = current_user.tasks.where('status_id = 3').count
    @tasks_overdue = current_user.tasks.where("due_at < ? AND status_id != ? AND has_priority = ?", Date.today, 3, false)
    @tasks_with_priority = current_user.tasks.where("has_priority = ? AND status_id != ?", true, 3)
    render :layout => 'pages'
  end

  def statistics
    @tasks = Task.all
    @customers = Customer.all
    @elevators = Elevator.all
    @assignments = Assignment.all
    @users = User.all
    @projects = Project.all
  end
end
