class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # ensure authorization happens on every action in the application
  # check_authorization

  layout :layout_by_resource

  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!
  before_filter :set_customers

  require 'csv'

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end


  private

  def set_customers
    @customers = Customer.all
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit( :email, :password, :password_confirmation, :current_password, :first_name, :last_name,  :role_ids => []) }
  end

  def layout_by_resource
    if devise_controller? && ( action_name == 'sign_in' or action_name == 'new')
      "devise"
    else
      "application"
    end
  end

end
