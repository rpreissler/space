class ProjectsController < ApplicationController

  load_and_authorize_resource #cancancan authorize method

  respond_to :html

  def index
    @projects = Project.all
    respond_with(@projects)
  end

  def show

    #@project_tasks = @project.tasks.all.map { |project| project.id }.join(" OR ")
  #  if @project_tasks.sum > 0
    #  @activities_by_projects = Activity.where("(targetable_type = 'Project' AND targetable_id = #{@project.id}) OR (targetable_type = 'Task' AND targetable_id = #{@project_tasks}) ").order("created_at DESC")
    #else
    # @activities_by_projects = Activity.where("targetable_type = 'Project' AND targetable_id = #{@project.id}").order("created_at DESC")
    #end

    respond_with(@project)
  end

  def new
    @project = Project.new
    #@project.build_document
    @project.documents.build
    respond_with(@project)
  end

  def edit
    @project.documents.build      # insert a new 'create document field'
  end

  def create
    @project = Project.new(project_params)
    if @project.save
      current_user.create_activity(@project, 'created') # create project activity
      redirect_to @project
    else
      render "new"
    end

  end

  def update
    @project.update(project_params)
    current_user.create_activity(@project, 'updated') # updated project activity
    respond_with(@project)
  end

  def destroy
    @project.destroy
    respond_with(@project)
  end

  private
    def set_project
      @project = Project.find(params[:id])
    end

    def project_params
      params.require(:project).permit(:name, :description, :start_date, :end_date, :planned_hours, :customer_id, documents_attributes: [:id, :brutto, :netto, :number, :due_at, :attachment, :supplier_id])
    end
end
